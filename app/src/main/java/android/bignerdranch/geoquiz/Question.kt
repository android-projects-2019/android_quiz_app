package android.bignerdranch.com

import androidx.annotation.StringRes

data class Question(@StringRes val textResId :Int,  // The '@stringRes' annotation leads to prevent runtime crash as it helps the code-inspector-Lint at compile time to verify that a valid string-resourceID is passed
                    val answer :Boolean,
                    var answered :Boolean = false)
// the integer is expected to be a String-Resource-Reference
// textResId variable will hold the resource ID (always an Int) of the string resource for a question.
