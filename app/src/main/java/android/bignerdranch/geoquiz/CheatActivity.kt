package android.bignerdranch.com

import android.app.Activity
import android.bignerdranch.geoquiz.R
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_cheat.*
import kotlinx.android.synthetic.main.activity_main.*

private const val TAG = "CheatActivity"
const val EXTRA_ANSWER_SHOWN = "com.bignerdranch.android.geoquiz.answer_shown"
private const val EXTRA_ANSWER = "com.bignerdranch.android.geoquiz.answer" // such a string helps to prevent name collision with 'extras' from other apps
// an activity can be started for several diff places, so keys for 'extras' should defined on the activites that retrieve & use them

class CheatActivity : AppCompatActivity() {
    private var answer = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "CheatActivity.onCreate()")
        setContentView(R.layout.activity_cheat)

        //<Intent-that-started-this-activity>.getBooleanExtra(String, Boolean)
        answer = intent.getBooleanExtra(EXTRA_ANSWER, false)

        /*
        the variable 'intent' contains the Intent that started this activity
        in java we can access that Intent using the getIntent function of the Activity
        the equivalent can be done in Kotlin by just accessing the variable as it is
        */
        _show_answer_button.setOnClickListener {
            _answer_text_view.setText( if(answer) R.string.true_button else R.string.false_button )
            setResult( //packages up result-code & intent; when user presses back, ActivityManager calls onActivityResult(requestCode-MainActivity, resultCode, data-CheatActivity)
                Activity.RESULT_OK, // resultCode :Int
                Intent().putExtra(EXTRA_ANSWER_SHOWN, true) // data :Intent
            )
            Log.d(TAG, "Data on Intent sent back to MainActivity ; setResult(" )
        }
        /*
        2 functions to send data back to parent
        setResult(resultCode :Int, data :Intent)
        setResult(resultCode :Int = Activity.RESULT_OK/Activity.RESULT_CANCELED/RESULT_FIRST_USER) RFU-as an offset when defining your own search result codes
        setting result codes is useful when the parent needs to take diff action depending on how the child activity finished
        */
    }
    /*
no reason for MainActivity, or any other code in your app, to know implementation details of what CheatActivity expects as 'extras' on its Intent
can encapsulate that work into a newIntent() fun Which allows to create an Intent properly configured the extras CheatActivity will need
using a newIntent(Context, <anyDataType>) function inside a companion object like this for activity subclasses will make it easy for other code to properly configure their launching intents
*/
    companion object {
        fun newIntent(packageContext : Context, answer :Boolean) : Intent {
            return Intent(packageContext, CheatActivity::class.java) // gives an Intent object
                .putExtra(EXTRA_ANSWER, answer) // 'extra'(=data) is put into that object
                // such many putExtra can be chained to as it returns the same Intent object on which it was configured on
        }
    }
    override fun onStart() {
        super.onStart()
//Toast.makeText(this, "Your activity got started \"onStart() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onStart() called")
    }
    override fun onResume() {
        super.onResume()
//Toast.makeText(this, "Your activity got focus \"onResume() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onResume() called")
    }
    override fun onPause() {
        super.onPause()
//Toast.makeText(this, "Your activity got paused \"onPause() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onPause() called")
    }
    override fun onStop() {
        super.onStop()
//Toast.makeText(this, "Your activity got stopped \"onStop() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onStop() called")
    }
    override fun onRestart() {
        super.onRestart()
//Toast.makeText(this, "Your activity got restarted \"onRestart() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onRestart() called")
    }
    override fun onDestroy() {
        super.onDestroy()
//Toast.makeText(this, "Your activity got destroyed \"onDestroy() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onDestroy() called")
    }
}

/*companion object
*  - allows to access functions without having an instance of a class
*  - replaces static members in java
*  - initialized when
*               - the containing class is initialized or
*               - one of its members is referenced
*/