package android.bignerdranch.com
import android.bignerdranch.geoquiz.R
import android.util.Log
import androidx.lifecycle.ViewModel
private const val TAG = "QuizViewModel"
/** contains the presentation logic
 * has all the data that its associated screen needs, formats it & makes it easy to access
 * A ViewModel stays in memory during a configuration change.
 * ViewModel does not shine in the process death scenario since it gets wiped away from memory along with the process and everything in it.
 * This is where saved instance state takes center stage.
 * Use saved instance state to store the minimal amount of information
necessary to re-create the UI state (for example, the current question
index). Use ViewModel to cache the rich set of data needed to
populate the UI in memory across configuration changes for quick and
easy access.
When the activity is re-created after process death, use
the saved instance state information to set up the ViewModel as if
the ViewModel and activity were never destroyed.

no easy way to determine whether an
activity is being re-created after process death versus a configuration
change. Why does this matter? A ViewModel stays in memory
during a configuration change. So if you use the saved instance state
data to update the ViewModel after the configuration change, you
are making your app do unnecessary work. If the work causes the user
to wait or uses their resources (like battery) unnecessarily, this
redundant work is problematic.
One way to fix this problem is to make your ViewModel a little
smarter. When setting a ViewModel value might result in more
work, first check whether the data is fresh before doing the work to
pull in and update the rest of the data:
 */
class QuizViewModel :ViewModel() {
    init { Log.d(TAG, "ViewModel instance created") }
    // this is actually model data
     private val questionBank = listOf(
        Question(R.string.question_australia, true),
        Question(R.string.question_oceans, true),
        Question(R.string.question_mideast, false),
        Question(R.string.question_africa, false),
        Question(R.string.question_americas, true),
        Question(R.string.question_asia, true))
    // remove the private access modifier so the property value can be accessed by external classes(MainActivity)
 /*private*/ var cI = 0 //(1..6).toList().shuffled().first()
                set(v) { if (field != v) field = v }
    var score = 0.0
        set(v) { if (field != v) field = v }
    var isCheater = false
    val currentQuestionAnswer :Boolean
        get() = questionBank[cI].answer
    val currentQuestionTextResId :Int
        get() = questionBank[cI].textResId
    fun moveToNext() { cI = (cI + 1) % questionBank.size }
    fun moveToPrev() { cI = if (cI == 0) cI else (cI -1) }
    fun moveToDoublePrev() { cI = if (cI in 0..1) 0 else cI - 2 }
    fun moveToDoubleNext()  { cI = (cI + 2) % questionBank.size}
    fun answered() { questionBank[cI].answered = true }
    val questionAnswered :Boolean
        get() = questionBank[cI].answered
    override fun onCleared(){   // place to perform any cleanup, such as unobserving a data source
        super.onCleared()
        Log.d(TAG, "ViewModel instance about to be destroyed ViewModel().onCleared() called")
    }
}