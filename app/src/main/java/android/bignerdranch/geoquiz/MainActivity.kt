package android.bignerdranch.com

import android.app.Activity
import android.app.ActivityOptions
import android.bignerdranch.geoquiz.R
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*

private const val TAG = "MainActivity"
private const val KEY_INDEX  = "index"
private const val REQUEST_CODE_CHEAT = 0
//  stashed -> save for future use
class MainActivity : AppCompatActivity() {
    init {
        Log.d(TAG, "MainActivity instance created")
    }
    /** // To associate the activity with an instance of QuizViewModel
     * 	ViewModelProviders provides instance of ViewModelProvider
     * 	ViewModelProvider
     * 	        - provides instance of ViewModel[QuizViewModel]
     * 	        - when queried by activity for the 1st time creates & returns a new asked ViewModel instance
     * 	        - [after a configuration change] when queried by activity for the 2nd time, the instance of ViewModel that was created the 1st is returned
     * Hence, to get an instance of QuizViewModel, a ViewModelProvider is needed which will come from a ViewModelProviders
     * val viewModelProvider : ViewModelProvider = ViewModelProviders.of(this) // creates & returns a ViewModelProvider associated with the activity
     * val quizViewModel: QuizViewModel = viewModelProvider.get(QuizViewModel::class.java) // returns an instance of QuizViewModel
     *
    val quizViewModel = ViewModelProviders.of(this).get(QuizViewModel::class.java) // this ViewModel(QuizViewModel) is now said to be 'scoped' to this activity's lifecycle, & ViewModel's lifecycle more closely mirrors the user's expectations: stays in memory during a configuration change & is destroyed when its activity is actually finished
    Log.d(TAG, "Got a QuizViewModel: $quizViewModel")
    Toast.makeText(this, "Got a QuizViewModel: $quizViewModel", Toast.LENGTH_SHORT).show()  */
    private val quizViewModel: QuizViewModel by lazy {
        ViewModelProviders.of(this).get(QuizViewModel::class.java)
    }

    /**Lazy benefits:
     * 1.Calculation & assignment will not happen until the variable/property accessed the first time
     * 2. In this case it is good as we can't access a ViewModel until its paired activity is set-up first(Activity.onCreate() not called)*/


    // in case Activity was not finished by the user by pressing back button the bundle object will be non-null, Activity record are not discarded
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) //Activity.onCreate(Bundle?) -> same function called on this object's-superclass(=Activity) & passed the bundle just received from the OS
        Log.d(TAG, "onCreate(Bundle?) called")  //Toast.makeText(this, "Your activity got created \"onCreate(Bundle?) called\"", Toast.LENGTH_SHORT).show()
        setContentView(R.layout.activity_main)   // ; Toast.makeText(this, "Your layout got inflated", Toast.LENGTH_SHORT).show()

        /**Bundle -> will contain the data this function can use to re-create the View from a saved state.
         * if you use the saved instance state data to update the ViewModel before the configuration change, you are making your app do unnecessary work*/
        savedInstanceState?.run {   // read it back in onCreate
            quizViewModel.cI = getInt(KEY_INDEX, 0) // if the key doesn't exist in the bundle set the value to 0
            quizViewModel.score = getDouble("score", 0.0)
        }

        updateQuestion()
        _score_board.text = "${quizViewModel.score}"

        // the listener will increment the index & update the TextView's text
        _question_text.setOnClickListener {
            quizViewModel.moveToNext()
            updateQuestion()
        }
        _true_button.setOnClickListener (object : View.OnClickListener { // a View.OnClickListener implementing object, created to respond to Click-event
            override fun onClick(v: View?) {
                checkAnswer(true)
            }
        })
//        {
//            checkAnswer(true)
//        }
        _false_button.setOnClickListener {
            checkAnswer(false)
        }
        _next_button.setOnClickListener {
            quizViewModel.moveToNext()
            updateQuestion()
        }

//        _prev_button.setOnClickListener {
//            quizViewModel.moveToPrev()
//            updateQuestion()
//        }

        _double_prev_button.setOnClickListener {
            quizViewModel.moveToDoublePrev()
            updateQuestion()
        }
        _double_next_button.setOnClickListener {
            quizViewModel.moveToDoubleNext()
            updateQuestion()
        }
        _cheat_button.setOnClickListener {
            /*
            // Start of another activity by this activity by requesting/passing an Intent to ActivityManager(=a part of the OS, creates Activity instance & calls its onCreate(Bundle?) function)
            val intentToStartCheatActivity = Intent(this, CheatActivity::class.java) //'this' argument here specifies the package the 'class' argument can be found in
            // before starting the 'class' argument, the ActivityManager checks the package's manifest for a declaration with the same name as the specified class
            // value can be send as an 'extra' on top of the same Intent
            */
            val intentCheatActivity = CheatActivity.newIntent(this@MainActivity , quizViewModel.currentQuestionAnswer)
            // the following code will not be found in < API23
            // Device's version of Android should be greater than or equal to Marshmallow/API23
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val options = ActivityOptions.makeClipRevealAnimation(it, 0, 0, it.width, it.height)
            startActivityForResult(
                    intentCheatActivity,
                    REQUEST_CODE_CHEAT, // a user defined int sent to the child-Activity & then received back by this parent-Activity; used when an activity starts more than one type of child activity & needs to know who is reporting back, if the child-Activity doesn't used its setResult(...) , then the OS sends a default result code Activity.RESULT_CANCELED
                    options.toBundle()
                )} else {
                startActivityForResult(intentCheatActivity, REQUEST_CODE_CHEAT)
            }
                Log.d(TAG, "Intent send to start CheatActivity ; startActivityForResult() called")
        }
    }

    /** Bundle -> to which it saves & retrieves its state
     * Save data of the viewModel so that When the activity is re-created (after user didn't pressed the back button to exit the app),
        saved-instance-state information can be used to set up the ViewModel as if the ViewModel and its owner/scope Activity were never destroyed.
     * Use saved instance state to store the minimal amount of information necessary to re-create the UI state (for example, the current question index).
       when its called by the system the data is saved to a Bundle object(-> stuffed in activity's "activity-record" by the OS) */
override fun onSaveInstanceState(savedInstanceState: Bundle) {
    super.onSaveInstanceState(savedInstanceState) ; Log.i(TAG, "onSaveInstanceState called")
        savedInstanceState.run {
            putInt(KEY_INDEX, quizViewModel.cI) //key-value pairs
            putDouble("score", quizViewModel.score) }
        // OS passes this stashed data in the bundle to onCreate(bundle?) ; The bundle may also contain additional information added by the framework, such as the contents of an EditText or other basic UI widget state.
}
    //MainActivity.starActivityForResult(Intent1, requestCodeM) -> ActivityManager -> OtherActivity.onCreate(savedInstanceState :Bundle?)
    //OtherActivity.setResult(resultCodeO, Intent2) -> ActivityManger -> MainActivity.onActivityResult(requestCodeM, resultCodeO, Intent2)
    override fun onActivityResult(requestCode :Int, resultCode :Int, data :Intent?) { // to pull the value out of the result sent back from CheatActivity
        super.onActivityResult(requestCode, resultCode, data)
        // check the resultCode & requestCode to be sure they are what you expect
        if (resultCode != Activity.RESULT_OK) return
        if (requestCode == REQUEST_CODE_CHEAT) {
            quizViewModel.isCheater = data?.getBooleanExtra(EXTRA_ANSWER_SHOWN, false) ?: false
            Log.d(TAG, "data from CheatActivity received ; onActivityResult() called")
        }

    }
    private fun checkAnswer(userAnswer: Boolean) {
        quizViewModel.answered()
        buttons()
        val messageResId = when {
            quizViewModel.isCheater -> {
                quizViewModel.score -= 5
                R.string.judgment_toast
            }
            userAnswer == quizViewModel.currentQuestionAnswer -> {
                quizViewModel.score++
                R.string.correct_toast
            }
            else -> {
                quizViewModel.score -= 0.5
                R.string.incorrect_toast
            }
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_LONG).show()
        _score_board.text = "${quizViewModel.score}"
        quizViewModel.isCheater = false
    }
    private fun updateQuestion() {
        //can create a brand new Exception and pass it to the Log.d(String, String, Throwable) without ever throwing it, and you will get a report of where  the exception was created in Logcat
        //Log.d(TAG, "Updating question text", Exception())
        val questionResId = quizViewModel.currentQuestionTextResId
        _question_text.setText(questionResId)
        buttons()
//        isFinishing // is a variable
    }
    private fun buttons(): Boolean {
        return if (!quizViewModel.questionAnswered) {
            _true_button.isEnabled = true
            _false_button.isEnabled = true
            true
        } else {
            _true_button.isEnabled = false
            _false_button.isEnabled = false
//            Toast.makeText(this, "Attempted", Toast.LENGTH_SHORT).show()
            false
        }
    }

    override fun onStart() {
        super.onStart()
//Toast.makeText(this, "Your activity got started \"onStart() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onStart() called")
    }
    override fun onResume() {
        super.onResume()
//Toast.makeText(this, "Your activity got focus \"onResume() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onResume() called")
    }
    override fun onPause() {
        super.onPause()
//Toast.makeText(this, "Your activity got paused \"onPause() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onPause() called")
    }
    override fun onStop() {
        super.onStop()
//Toast.makeText(this, "Your activity got stopped \"onStop() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onStop() called")
    }
    override fun onRestart() {
        super.onRestart()
//Toast.makeText(this, "Your activity got restarted \"onRestart() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onRestart() called")
    }
    override fun onDestroy() {
        super.onDestroy()
//Toast.makeText(this, "Your activity got destroyed \"onDestroy() called\"", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onDestroy() called")
    }

    fun prev(v :View) {
        quizViewModel.moveToPrev()
        updateQuestion()
    }
}






/**
this.finish()   // ViewModel-Activity pair is removed from memory
When your activity is stashed, an Activity object does not exist, but the activity record object lives on in the OS.
The OS can reanimate the activity using the activity record when it needs to.
Activity records are discarded on reboot & when the app finishes officially
Activity records/savedInstanceState are only not discarded either on reboot too, if the user pressed home button to exit app

Toast.makeText(this, R.string.correctToast, Toast.LENGTH_SHORT) // a static function, creates & configures a Toast object
Context is needed by the Toast class to be able to find and use the string's resource ID
context parameter need a type of Context, it takes `this-Activity` as Acitivity is a subclass of Context


setContentView(R.layout.activity_main)  // an integer-constant(=activity_main) within the 'layout' inner class of 'R' class
//Interfaces in Java with a Single-Abstract-Function are handled with a function-literal/lambda-exp which gets turned into an object that implements the listener-interface, View.OnClickListener corresponding to the button-click event, that the listener is listening for; this object is our listener(responds to a events, initiated by user,OS or another app)
true_button.setOnClickListener{checkAnswer(true)}

 */